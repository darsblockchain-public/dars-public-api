<?php
require 'DarsApi.php';


//Внимание!! Для возможности создавать аккаунты бесплатно вы должны связаться с командой DARS
// и подать заявку сообщив имя своего аккаунта в данном случае darsuser1111. 
// иначе с вашего аккаунта будут списаны токены DRS в эквиваленте 4$ за создании нового аккаунта.

function test(){

    $myAccount="darsuser1111";
    $myPrivateKey="5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y";
    //TESTNET https://blockchain2.dars.one
    $testNet = new DarsApi('https://blockchain2.dars.one');
    try {
        //получение размеров комиссий по номеру команды
        //20 отправка токенов
        //11 создание кошелька для всех кто не является привилегированным 'создателем'
        //40 сервисная команда для компаний(запись в журнал)
        $commission=$testNet->getСommission(20);
        if($commission->error){throw new Exception($commission->error);}
        //Генерация промокода для оплаты регистрации блокчейн аккаунта DARS
        //с аккаунта $myAccount будут списаны токены DRS в эквиваленте 4$
        $newPromo=$testNet->newPromoCode($myAccount,$myPrivateKey);
        if($newPromo->error){
            throw new Exception($newPromo->error);
        }else{
            print_r("newPromoCode: " . $newPromo->promoCode . "\n");
        }
        //создание аккаунта
        $newAcc=$testNet->newRandomAccount($myAccount,$myPrivateKey);
        if($newAcc->error){throw new Exception($newAcc->error);}
        //print_r($newAcc);
        //Если аккаунт создался то пополним его на 20 DRS
        $toUser=$testNet->sendTokens($myAccount,$newAcc->name, "30.00000000 DRS", "пополнение баланса",$myPrivateKey);
        if($toUser->error){throw new Exception($toUser->error);}
        $checTimer = new EvTimer(0, 1, function ($timerResult) use( $testNet,$toUser) {
            //$timerResult->data++;
            //echo $timerResult->data . PHP_EOL;
            $Checker = $testNet->checkTransaction($toUser->block_num,$toUser->transaction_id);
            if($Checker->error){
                echo "Test fail.\n";
                $timerResult->stop();
            }
            if($Checker->status!="waiting"){
                print_r($Checker);
                $timerResult->stop();
            }                     
        });
        //print_r($toUser);
        //Если транзакция без ошибок то проверим баланс DRS токенов нового пользователя
        $BAL=$testNet->getBalance($newAcc->name,"DRS");
        if($BAL->error){throw new Exception($BAL->error);}
        //print_r($BAL);
        if(!floatval($BAL->balance)>0){throw new Exception("current balance iz 0");}
        //обнулим аккаунт, вернём токены назад( за вычетом комиссии за транзакцию, которая будет снята с отправителя)
        $qty=round($BAL->balance-$commission->drs_commission,8,PHP_ROUND_HALF_UP) . " DRS";
        $fromUser=$testNet->sendTokens($newAcc->name,$myAccount, $qty, "возврат",$newAcc->activePrivatKey);       
        if($fromUser->error){throw new Exception($fromUser->error);}
        //print_r($fromUser);
        $checTimer2 = new EvTimer(0, 1, function ($timerResult) use( $testNet,$fromUser) {
            $Checker = $testNet->checkTransaction($fromUser->block_num,$fromUser->transaction_id);
            if($Checker->error){
                echo "Test fail.\n";
                $timerResult->stop();
            }
            if($Checker->status!="waiting"){
                print_r($Checker);
                $timerResult->stop();
            }                     
        });        
        //Проверим баланс ещё раз, он должен быть равен 0
        $BAL=$testNet->getBalance($newAcc->name,"DRS");
        if($BAL->error){throw new Exception($BAL->error);}
        //print_r($BAL);
        if($BAL->balance == '0'){
            Ev::run(); 
            echo "Test is OK.\n";
        }else{
            echo "Test fail.\n";
        }        
        
    }catch (Exception $e) {
        echo $e->getMessage();
        die();
    }
     
}

test();
