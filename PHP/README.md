### Пакет консольных утилит для отправки транзакций `dars-push` и offline генерации ключей `dars-key` необходимо предварительно установить. 

#### Устанавливаем пакет на Ubuntu-16.04 :
```bash
git clone https://gitlab.com/darsdeveloper9/dars-public-api.git
cd dars-public-api
apt-get update
sudo apt install curl
sudo apt install -f -y ./dars-tools_ubuntu-16.04_amd64.deb
```
#### Удаляем пакет:
```bash
sudo apt-get remove --purge -y dars-tools
```
`Смотрите примеры использования в example.php`
```bash
# --- EvTimer ---
#sudo apt install php7.0-dev
#sudo apt install php-pear
#sudo pecl install ev
#sudo echo 'extension=ev.so' > /etc/php/7.0/cli/php.ini
#sudo service nginx restart

git clone https://gitlab.com/darsdeveloper9/dars-public-api.git
cd dars-public-api/PHP
php example.php
```