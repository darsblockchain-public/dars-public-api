'use strict';
const { Api, JsonRpc, RpcError} = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const ecc = require('eosjs-ecc')



/**
 * @module DarsApi
 * @param {string} url - адресс сети (тестовая или MainNet)
 *
 * @example 
 * //Инициализация
 * //js
 * const darsApi = require('./DarsApi.js');
 * testNet=new darsApi("https://blockchain2.dars.one");
 * //PHP
 * require 'DarsApi.php';
 * $testNet = new DarsApi('https://blockchain2.dars.one');
 */
 
module.exports = class DarsApi extends Api{

    constructor(url) {       
        super({ rpc: new JsonRpc(url, { fetch }), 
        signatureProvider:new JsSignatureProvider(["5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y"]),//default PrivateKey
         textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });     
    }

    randomName() {
        const first = "abcdefghijklmnopqrstuvwxyz";
        const possible = "abcdefghijklmnopqrstuvwxyz12345";
        var name = "";
        name += first.charAt(Math.floor(Math.random() * first.length));
        
        for (var i = 0; i < 11; i++){
          name += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return name;
    }
    
    
/**
 * 
  * @description
  * Генерация и регистрация аккаунта **newRandomAccount** ( creator, privateKey )
  * 
  * Внимание!! Для возможности создавать аккаунты бесплатно вы должны связаться с командой DARS
  * и подать заявку сообщив имя своего аккаунта в данном случае darsuser1111 (config.blockchain.actor). 
  * иначе с вашего аккаунта будут списаны токены DRS в эквиваленте 4$ за создании нового аккаунта.
  * @param {string} creator - аккаунт инициатора транзакции, который платит за создание кошелька
  * @param {string} privateKey - приватный ключ отправителя
  * @example
  * testNet.newRandomAccount("user11111111","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
  * @returns Promise<object>
  * @example
  * Promise<{
  *  block_num: number,//номер блока в который попала транзакция
  *  transaction_id: number,
  *  name: string, //имя созданного аккаунта
  *  activePrivatKey: string,//Приватный ключ аккаунта для подписи транзакций
  *  ownerPrivatKey: string,//Приватный ключ владельца аккаунта
  *  error: any}>
  */ 
    async newRandomAccount(creator,privateKey) {
        //генерируем ключи
        this.signatureProvider=new JsSignatureProvider([privateKey]);
        const activePrivatKey=ecc.randomKey();//Активный ключ для подписи транзакций от лица нового аккаунта
        const ownerPrivatKey=ecc.randomKey();//Ключ владельца аккаунта для возможности замены активного ключа
        const newName=this.randomName();//Рандомное имя аккаунта
        let result={
          block_num: 0,
          transaction_id: 0,
          name: "",
          activePrivatKey: "",
          ownerPrivatKey: "",
          error: null      
        };
      
        return Promise.all([activePrivatKey, ownerPrivatKey,newName]).then(async (values)=> {
      
          const activePublicKey = ecc.privateToPublic(values[0]);
          const ownerPublicKey = ecc.privateToPublic(values[1]);
          result.name=values[2];
          result.activePrivatKey=values[0];
          result.ownerPrivatKey=values[1];

          return this.transact({
            actions: [{
              account: 'dars.signup',
              name: 'makedrsacc',
              authorization: [{
                actor: creator,// ваш аккаунт в сети DARS
                permission: 'active',
              }],
              data: {
                from: creator, //создатель аккаунта. За создание будет снята плата,если вы не добавлены в список привелигированых партнёров
                acc_data: {
                  field_0: result.name,//Имя аккаунта который мы пытаемся создать
                  field_1: ownerPublicKey,//Публичный ключ владельца нового аккаунта
                  field_2: activePublicKey,//Публичный активный ключ нового аккаунта
                },
              },
            }]
          }, {
            blocksBehind: 3,
            expireSeconds: 30,
          }).then(async (info) => {
            result.block_num=info.processed.block_num;
            result.transaction_id=info.transaction_id;
            return result;
          }).catch(async (err) => {
            result.error=err;
            return result;
          });
        }).catch(async (err) => {
          result.error=err;
          return result;
          }); 
    };

/**
  * @description
  * Отправка токенов  **sendTokens** ( from, to, quantity, memo,privateKey )
  * @param {string} from - аккаунт отправителя
  * @param {string} to - аккаунт получателя
  * @param {string} quantity - количество и токенов
  * @param {string} memo - коментарий транзакции
  * @param {string} privateKey - приватный ключ отправителя
  * @example
  * testNet.sendTokens("user11111111","user2222222222", "100.00000000 DRS", "оплата","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
  * @returns Promise<object>
  * @example
  * Promise<{
  *  block_num: number;
  *  transaction_id: number;
  *  from: string;
  *  to: string;
  *  quantity: string;
  *  error: any;
  * }>
  * 
  */  
    async sendTokens(from,to,quantity,memo,privateKey) {
        this.signatureProvider=new JsSignatureProvider([privateKey]);
        let result={
          block_num: 0,
          transaction_id: 0,
          from: from,
          to: to,
          quantity: quantity,
          error: null      
        };
        return this.transact({
          actions: [{
            account: 'dars.invoice',
            name: 'user2user',
            authorization: [{
              actor: from,
              permission: 'active',
            }],
            data: {
              from: from, 
              to: to,
              quantity: quantity,
              memo: memo,
            },
          }]
        }, {
          blocksBehind: 3,
          expireSeconds: 30,
        }).then(async (info) => {
          result.block_num=info.processed.block_num;
          result.transaction_id=info.transaction_id;
          return result;
        }).catch(async (err) => {
          result.error=err;
          return result;
        });
      
    };    

/**
  * @description
  * Получение баланса аккаунта по конкретному токену **getBalance** ( account , symbol )
  * @param {string} account - аккаунт по которому запрашиваем баланс
  * @param {string} symbol - символ токена по которому хотим получить баланс
  * @example
  * testNet.getBalance("user11111111","DRS"); 
  * @returns Promise<object>
  * @example
  * Promise<{
  *  account: string;
  *  symbol: string;
  *  balance: any;
  *  error: any;
  * }>
  * @example
  * { account: 'user11111111',
  *   symbol: 'DRS',
  *   balance: '0',
  *   error: null }
  * 
  */      
    async getBalance(account,symbol) {
        let accBalance={
          account:account,
          symbol:symbol,
          balance: undefined,
          error: null
        }
        return this.rpc.get_table_rows({
          json: true,
          code: 'dars.token',
          scope: account,
          table: 'accounts'
        }).then(async result=>{
          let balanceRow = result.rows.find(row => (row.balance.indexOf(symbol))!=-1);//ищем токен
          return Promise.all([balanceRow ? balanceRow.balance.split(' ')[0]:'0']);
        }).then(async result=>{
          accBalance.balance=result[0];
          return accBalance;
        }).catch (async error=> {
          accBalance.error=error;
          return accBalance;
        });  
    };

/**
  * @description
  * Получение текущей комиссии  **getСommission** ( cmd )
  * 
  * Комиссия оплачивается в токенах DRS и её размер 
  * может менятся при изменении курса DRS.
  * Оплачивает комиссию инициатор транзакции(from)
  * @param {number} cmd - номер команды по которой запрашиваем комиссию
  * @example
  * //20 отправка токенов
  * //11 создание кошелька для всех кто не является привелигированым 'создателем' 
  * //40 сервисная команда для компаний(запись в журнал)
  * testNet.getСommission(20); 
  * @returns Promise<object>
  * @example
  * Promise<{
  *  exchange_rate: any;
  *  usd_amount: any;
  *  drs_commission: any;
  *  error: any;
  * }>
  * @example
  * { exchange_rate: '0.07000000',
  *   usd_amount: '0.05',
  *   drs_commission: '0.71428571',
  *   error: null }
  */ 
    async getСommission(cmd) {

      let commission={
        exchange_rate: undefined,
        usd_amount:undefined,
        drs_commission: undefined,
        error: null
      }
      return this.rpc.get_table_rows({
        json: true,
        code: 'dars.invoice',
        scope: 'dars.invoice',
        table: 'exchrate'
      }).then(async result=>{
        commission.exchange_rate=parseFloat(result.rows[0].rate).toFixed(8);
        return this.rpc.get_table_rows({
        json: true,
        code: 'dars.invoice',
        scope: 'dars.invoice',
        table: 'feeusd',
        key_type: 'i64',
        lower_bound: cmd,
        upper_bound: cmd,
        limit: 1
        })
      }).then(async result=>{
        commission.usd_amount=parseFloat(result.rows[0].fee).toFixed(2);
        commission.drs_commission=parseFloat(commission.usd_amount / commission.exchange_rate).toFixed(8);
        return commission;
      }).catch(async error=>{
        commission.error=error;
        return commission;
      });
    };


/**
  * @description
  * Проверка статуса транзакции **checkTransaction** ( block_num, id )
  * 
  * Транзакция считается принятой если её блок меньше чем номер последнего необратимого блока
  * и её статус 'executed'
  * @param {number | string} block_num - номер блока транзакции
  * @param {string} id - id транзакции
  * @example
  * testNet.checkTransaction(1234,"754f4dc837866e821a90eaf73314012cd208ebdb712f28555aad3d9a0e4bf992"); 
  * @returns Promise<object>
  * @example
  * Promise<{
  *  id: string;
  *  status: string;
  *  error: any;
  * }>
  * @example
  * { id:
  *   '754f4dc837866e821a90eaf73314012cd208ebdb712f28555aad3d9a0e4bf992',
  *   status: 'executed',
  *   error: null }
  * 
  */     
    async checkTransaction(block_num,id) {
      let LI={
        id: id,
        status:"waiting",
        error: null
      }
      return this.rpc.get_info().then(async result=>{
        if(result.last_irreversible_block_num>block_num){
          return this.rpc.get_block(block_num);
        }else{
          return null;
        }
      }).then(async result=> {
        if(result){
          result=result.transactions.find(transaction => transaction.trx.id == id);
          if(!result){throw "Transaction not found";}
        }
        return Promise.all([result ? result.status:'waiting']);
      }).then(async result=>{
        LI.status=result[0];
        return LI;
      }).catch (async error=> {
        //if (error instanceof RpcError){
          //console.log(JSON.stringify(error.json, null, 2));
        //}         
        LI.error=error;
        return LI;
      });       
  };


/**
  * @description
  * Генерация промокода для оплаты регистрации блокчейн аккаунта DARS **newPromoCode** ( creator, privateKey )
  * с аккаунта creator будут списаны токены DRS в эквиваленте 4$
  * @param {string} creator - аккаунт инициатора транзакции, который платит за генерацию промокода
  * @param {string} privateKey - приватный ключ отправителя
  * @example
  * testNet.newPromoCode("user11111111","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
  * @returns Promise<object>
  * @example
  * Promise<{
  *  block_num: number,//номер блока в который попала транзакция
  *  transaction_id: number,
  *  promoCode: string, //промокод
  *  error: any}>
  */  

  async newPromoCode(creator,privateKey) {
    //генерируем ключи
    this.signatureProvider=new JsSignatureProvider([privateKey]);
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var promoCode = "";  
    for (var i = 0; i < 24; i++){
      promoCode += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    let result={
      block_num: 0,
      transaction_id: 0,
      promoCode: "",
      error: null      
    };
  
    return Promise.all([promoCode]).then(async (values)=> {
  
      result.promoCode=values[0];

      return this.transact({
        actions: [{
          account: 'dars.signup',
          name: 'createpromo',
          authorization: [{
            actor: creator,// ваш аккаунт в сети DARS
            permission: 'active',
          }],
          data: {
            creator: creator,
            hash: ecc.sha256(values[0]),
          },
        }]
      }, {
        blocksBehind: 3,
        expireSeconds: 30,
      }).then(async (info) => {
        result.block_num=info.processed.block_num;
        result.transaction_id=info.transaction_id;
        return result;
      }).catch(async (err) => {
        result.promoCode="";
        result.error=err;
        return result;
      });
    }).catch(async (err) => {
      result.error=err;
      return result;
      }); 
};



 }