const fs = require('fs');
const config = JSON.parse(fs.readFileSync(__dirname + "/config.json"));
const darsApi = require('./DarsApi.js');

async function waitAccepted(f){

  f().then(async(result)=>{
    if (result.error == null) {

      if(result.status=="waiting"){
        setTimeout(() => waitAccepted(f), 1000);
      }else{
        console.log(result);
      }
    }else{
      console.log(result);
    }
    
  })
}


function test(){
  // TestNet endPoint
  testNet=new darsApi("https://blockchain2.dars.one");
  let newAcc;
  let commission;
//получение размеров комиссий по номеру команды
//20 отправка токенов
//11 создание кошелька для всех кто не является привилегированным 'создателем'
//40 сервисная команда для компаний(запись в журнал)
testNet.getСommission(20).then(result => {
  //console.log(result);
  if (result.error == null) {
    commission=result.drs_commission;
  }
}).catch (error=> {
  console.error(error.message);
});

testNet.newPromoCode(config.blockchain.actor,config.blockchain.private_key).then(result => {
  if (result.error == null) {
    console.log("newPromoCode: ",result.promoCode);
  }else{
    console.log("newPromoCode: ",result.error);
  }
}).catch (error=> {
  console.error("newPromoCode: ",error.message);
});  

testNet.newRandomAccount(config.blockchain.actor,config.blockchain.private_key).then(result => {
    //console.log(result)
    //Если аккаунт создался то пополним его на 20 DRS
    if (result.error == null) {
      newAcc=result;    
      return testNet.sendTokens(config.blockchain.actor,newAcc.name, "30.00000000 DRS", "пополнение баланса",config.blockchain.private_key);
    }
    throw new Error(result.error);

  }).then(result => {
    //console.log(result)
    var f = testNet.checkTransaction.bind(testNet,result.block_num,result.transaction_id);
    setTimeout(() => waitAccepted(f), 1000);//1 сек. повтор    
    //Если транзакция без ошибок то проверим баланс DRS токенов нового пользователя
    if (result.error == null) {
      return  testNet.getBalance(newAcc.name,"DRS");     
    }
    throw new Error(result.error);

  }).then(result => {
    //console.log(result)

    if (result.error == null) {
      if(parseFloat(result.balance)>0){
        //обнулим аккаунт, вернём токены назад( за вычетом комиссии за транзакцию, которая будет снята с отправителя)
        let qty=parseFloat(result.balance-commission).toFixed(8) +" DRS";
        return testNet.sendTokens(newAcc.name,config.blockchain.actor, qty, "возврат",newAcc.activePrivatKey);
      }else{
        throw new Error('current balance iz 0');
      }
    }
    throw new Error(result.error);

  }).then(result => {
    //console.log(result)
    if (result.error == null) {
      var f = testNet.checkTransaction.bind(testNet,result.block_num,result.transaction_id);
      setTimeout(() => waitAccepted(f), 1000);//1 сек. повтор
      //Проверим баланс ещё раз, он должен быть равен 0
      return  testNet.getBalance(newAcc.name,"DRS"); 
    }
    throw new Error(result.error);
  }).then(result => { 
    //console.log(result)
    if (result.error == null) {
      if(result.balance == '0'){
        return console.log("Test is OK.");
      }else{
        return console.log("Test fail.");
      }
    }
    throw new Error(result.error);     
  }).catch (error=> {
    console.error(error.message);
  });
}

test()

